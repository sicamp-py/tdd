import pytest

from .mars_rover import Rover, Map, Position

@pytest.fixture()
def position():
    return Position(5, 5, 'N')

@pytest.fixture()
def default_map():
    return Map(10, 10)

def test_init_sets_position(position, default_map):
    rover = Rover(default_map, position)
    assert rover.get_position() == position

@pytest.mark.parametrize("start_position, expected_direction", [
    (Position(5, 5, 'N'), 'W'),
    (Position(5, 5, 'S'), 'E'),
    (Position(5, 5, 'W'), 'S'),
    (Position(5, 5, 'E'), 'N'),
])
def test_turn_left_changes_direction(start_position,
                    expected_direction, default_map):
    rover = Rover(default_map, start_position)
    rover.turn_left()
    assert rover.get_position().direction == expected_direction


def test_turn_left_saves_position(default_map):
    start_position = Position(5, 6, 'N')
    rover = Rover(default_map, start_position)
    rover.turn_left()

    pos = rover.get_position()
    assert (pos.x, pos.y) == (5, 6)


@pytest.mark.parametrize("start_position, expected_position", [
    (Position(5, 6, 'N'), Position(4, 6, 'N')),
    (Position(5, 6, 'E'), Position(5, 7, 'E')),
    (Position(5, 6, 'S'), Position(6, 6, 'S')),
    (Position(5, 6, 'W'), Position(5, 5, 'W')),
])
def test_move_forward(start_position, expected_position, default_map):
    rover = Rover(default_map, start_position)
    rover.move_forward()

    assert rover.get_position() == expected_position

@pytest.mark.parametrize("start_position, expected_position", [
    (Position(0, 2, 'N'), Position(3, 2, 'N')),
    (Position(3, 4, 'E'), Position(3, 0, 'E')),
    (Position(3, 1, 'S'), Position(0, 1, 'S')),
    (Position(1, 0, 'W'), Position(1, 4, 'W')),
])
def test_moves_over_tor(start_position, expected_position):
    map = Map(4, 5)
    rover = Rover(map, start_position)

    rover.move_forward()

    assert rover.get_position() == expected_position
