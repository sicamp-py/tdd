from collections import namedtuple


class Map:
    def __init__(self, height, width):
        self.height = height
        self.width = width

Position = namedtuple('Position', ['x', 'y', 'direction'])

class Rover:
    _DIRECTIONS = ['N', 'E', 'S', 'W']
    _FORWARD_MOVES = {
        'N': (-1, 0),
        'E': (0, 1),
        'S': (1, 0),
        'W': (0, -1),
    }

    def __init__(self, map, start_position):
        self._map = map
        self._position = start_position

    def get_position(self):
        return self._position

    def turn_left(self):
        x, y, direction = self._position
        new_direction = self._DIRECTIONS[self._DIRECTIONS.index(direction)-1]
        self._position = Position(x, y, new_direction)

    def move_forward(self):
        x, y, direction = self._position
        dx, dy = self._FORWARD_MOVES[direction]
        new_x = (x + dx) % self._map.height
        new_y = (y + dy) % self._map.width
        self._position = Position(new_x, new_y, direction)

